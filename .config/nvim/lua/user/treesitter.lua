--------------------
-- Custom Parsers --
--------------------

--[[
local parser_config = require "nvim-treesitter.parsers".get_parser_configs()

parser_config.ttcl = {
  install_info = {
    url = "~/projects/tree-sitter-verilog",
    files = {"src/parser.c"},
    branch = "master",
    generate_requires_npm = false,
    requires_generate_from_grammer = false,
  },
  filetype="ttcl"
}
--]]

--[[
parser_config.tcl = {
  install_info = {
    url = "~/tree-sitter-tcl/tcl",
    files = {"src/parser.c"},
    branch = "master",
    generate_requires_npm = false,
    requires_generate_from_grammer = false,
  },
  filetype="tcl"
}
--]]

--[[
parser_config.tcl = {
  install_info = {
    url = "~/projects/tree-sitter-tcl/",
    files = {"src/parser.c"},
    branch = "main",
    generate_requires_npm = false,
    requires_generate_from_grammer = false,
  },
  filetype="tcl"
}


parser_config.tclsh = {
  install_info = {
    url = "~/tree-sitter-tcl/tclsh/",
    files = {"src/parser.c"},
    branch = "master",
    generate_requires_npm = false,
    requires_generate_from_grammer = false,
  },
}
--]]
local ft_to_parser = require"nvim-treesitter.parsers".filetype_to_parsername
ft_to_parser.ttcl = "ttcl"

local status_ok, configs = pcall(require, 'nvim-treesitter.configs')
if not status_ok then
  return
end
--
-----------------------
-- Treesitter Config --
-----------------------

configs.setup {
  ensure_installed = {'lua', 'c', 'cpp', 'yaml', 'json', 'verilog', -- the verilog one is incredibly slow 
                      'bash', 'latex', 'make', 'html', 'css', 'ttcl'},
  sync_install = true,
  ignore_install = {""},
  highlight = {
    enable = true,
    disable = {""}, -- list of lang to disable treesitter for
    additional_vim_regex_highlighting = true,
  },

  indent = { enable = true, disable = {"yaml"} },
  --  nvim-ts-rainbow config
  rainbow = {
    enable = false,
    disable = {"systemverilog",}, -- list of langs to disable the plugin for
    extended_mode = true, -- highlight non bracket delimeters, e.g. html tags
    max_file_lines = nil,
  },
  -- playground config, lists the tree sitter AST for a file, and highlights associated lines
  -- TSHighlightCapturesUnderCursor, is also useful for this purpose
  playground = {
    enable = false,
  },
  -- This is the best fucking shit
  refactor = {
    highlight_definitions = {
      enable = true,
      -- set to false if you have an updatetime of ~100ms
      clear_on_cursor_move = true,
    },
    highlight_current_scope = { enable = false },
    smart_rename = {
      enable = true,
      keymaps = {
        smart_rename = 'grr'
      },
    },
    navigation = {
      enable = true,
      keymaps = {
        goto_definition = 'gnd',
        list_definitions = 'gnD',
        list_definitions_toc = 'gO',
        goto_next_usage = 'gnn',
        goto_previous_usage = 'gnp',
      },
    },
  },
}

--------------------------------------
-- Language Specific Configurations --
--------------------------------------

-- highlight the edge_detectors the same way that types are highlighted
vim.api.nvim_set_hl(0, "@attribute.verilog", {link = "type"})
