local opts = {
  settings = {
    pylsp = {
      plugins = {
        pycodestyle = {
          ignore = {'W391'},
          maxLineLength = 150
        }
      }
    }
  }
}
return opts
