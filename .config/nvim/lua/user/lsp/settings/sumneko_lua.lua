local util = require 'lspconfig.util'

local root_files = {
  '.luarc.json',
  '.luacheckrc',
  '.stylua.toml',
  'stylua.toml',
  'selene.toml',
}

local opts = {
  single_file_support = true,
  cmd = { 'lua-language-server' },
  filetypes = { "lua" },
  root_dir = function(fname)
    return util.root_pattern(unpack(root_files))(fname) or util.find_git_ancestor(fname)
  end,
  settings = {
  		Lua = {
  			diagnostics = {
  				globals = { "vim" },
  			},
  			workspace = {
  				library = {
            vim.api.nvim_get_runtime_file("", true)
  					--[vim.fn.expand("$VIMRUNTIME/lua")] = true,
  					--[vim.fn.stdpath("config") .. "/lua"] = true,
  				},
  			},
        telemetry = {
          enable = false,
        },
  		},
  	},
  }
return opts
