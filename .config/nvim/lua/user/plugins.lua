-- using packer for plugin management
local fn      = vim.fn
local command = vim.cmd

-- Automatically install packer, if not present
local install_path = fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = fn.system {
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  }
  print "Installing packer, please close and reopen Neovim"
  command [[packeradd packer.nvim]]
end

-- Autocommand that reloads neovim whenever save plugins.lua file, TODO convert to pure lua
command [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup endk
]]

-- get instance of packer
local packer = require('packer')

-- Have Packer use a popup window for info
packer.init {
  display = {
    open_fn = function()
      return require("packer.util").float {border = "rounded"}
    end,
  },
}

return packer.startup(function(use)
  use 'wbthomason/packer.nvim'
  -- necessary for some other plugins apparantly
  use 'nvim-lua/popup.nvim'
  use 'nvim-lua/plenary.nvim'

  -- use 'morhetz/gruvbox'
  use 'ellisonleao/gruvbox.nvim' -- reimplementation in lua with treesitter support
  use 'savq/melange'
  use 'jiangmiao/auto-pairs'
  use 'nvim-lualine/lualine.nvim'

  -- cmp for completion
  use 'hrsh7th/nvim-cmp'
  use 'hrsh7th/cmp-buffer'
  use 'hrsh7th/cmp-path'
  use 'hrsh7th/cmp-cmdline'
  use 'saadparwaiz1/cmp_luasnip'
  use 'hrsh7th/cmp-nvim-lsp'
  use 'hrsh7th/cmp-nvim-lua'

  -- snippets
  use 'L3MON4D3/LuaSnip'
  use 'rafamadriz/friendly-snippets' -- lots of snips for various langs

  use {
    'scalameta/nvim-metals',
    requires = {"nvim-lua/plenary.nvim"},
  }

  -- lsp
  use 'neovim/nvim-lspconfig'
  use 'williamboman/nvim-lsp-installer'

  -- Git
  use 'lewis6991/gitsigns.nvim'

  use {
    'akinsho/bufferline.nvim',
    requires = {'kyazdani42/nvim-web-devicons'},
  }

  -- Treesitter
  ---[[
  use {'nvim-treesitter/nvim-treesitter',}

  use {
    'p00f/nvim-ts-rainbow',
    requires = 'nvim-treesitter',
    --after = 'nvim-treesitter',
  }

  use {
    'nvim-treesitter/playground', -- useful for deving treesitter
    requires = 'nvim-treesitter',
  }

  use {
    'nvim-treesitter/nvim-treesitter-refactor',
    requires = 'nvim-treesitter',
  }

  use {
    'nvim-telescope/telescope.nvim',
    tag = '0.1.0',
    requires = { {'nvim-lua/plenary.nvim'} }
  }
  --]]
  use { 'lewis6991/tree-sitter-tcl', run = 'make' }

  use 'lewis6991/impatient.nvim'

  use 'jose-elias-alvarez/null-ls.nvim'

  -- show indent column lines
  use 'lukas-reineke/indent-blankline.nvim'

  use 'nvim-tree/nvim-tree.lua'

  if PACKER_BOOTSTRAP then
    require('packer').sync()
  end
end)
