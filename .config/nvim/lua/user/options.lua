local global  = vim.g
local option  = vim.opt 
local command = vim.cmd

global.mapleader = ' '

command([[syntax enable]])
command([[filetype plugin indent on]])
option.ttimeoutlen   = 5
option.cmdheight     = 1
option.autochdir     = true
option.ignorecase    = true
option.showmode      = false
option.number        = true
option.expandtab     = true
option.tabstop       = 2
option.shiftwidth    = 2
option.wrap          = false
option.clipboard     = 'unnamedplus'
option.completeopt   = { 'menuone', 'noselect' }
option.conceallevel  = 0
option.swapfile      = false
option.splitbelow    = true
option.splitright    = true
option.cursorline    = true
option.scrolloff     = 8
option.termguicolors = true
option.undofile      = true   -- enable persistant undo
option.lazyredraw    = true
option.iskeyword:append('-')  -- '-', Finally support for kebab case!
option.formatoptions = 'jnqlro'
option.list = true;
option.listchars:append "eol:↴"
option.relativenumber = true
require("indent_blankline").setup {
  show_end_of_line = true;
}

-- Scala Metals plugin requirement
vim.opt_global.shortmess:remove("F")

-- remove builtin plugins I don't use
local disabled_built_ins = {
  'netrw',
  'netrwPlugin',
  'netrwSettings',
  'netrwFileHandlers',
  'gzip',
  'zip',
  'zipPlugin',
  'tar',
  'tarPlugin',
  'getscript',
  'getscriptPlugin',
  'vimball',
  'vimballPlugin',
  '2html_plugin',
  'logipat',
  'rrhelper',
  'spellfile_plugin',
  'matchit',
}

for _, plugin in pairs(disabled_built_ins) do
  global['loaded' .. plugin] = 1
end


command([[autocmd VimResized * wincmd =]])

-- TL Verilog Stuff (find out equivalent in true lua)
command([[au BufRead,BufNewFile *.tlv set filetype=tl-verilog]])
command([[autocmd Filetype tl-verilog setlocal tabstop=3]])
command([[autocmd Filetype tl-verilog setlocal shiftwidth=3]])
command([[filetype plugin indent on]])
vim.filetype.add({extension = { ttcl = 'ttcl' }})
